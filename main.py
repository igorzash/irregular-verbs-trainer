import curses
import requests
from random import shuffle, randint


def refresh(stdscr, pad):
    """ Refreshes pad """
    stdscr.refresh()
    pad.refresh(0, 0, 5, 5, 10, 60)


def message(stdscr, pad, *args):
    """ Clears pad and prints list of args """
    pad.clear()

    if len(args) > 0:
        for arg in args:
            # if str, simply print
            if isinstance(arg, str):
                pad.addstr(arg)
            # if tuple, print using specified color pair
            elif isinstance(arg, tuple):
                pad.addstr(arg[0], curses.color_pair(arg[1]))
            # else raise an error
            else:
                raise TypeError('Unknown type')

    refresh(stdscr, pad)


def fetch_data():
    """ Fetches data from api """
    r = requests.get('https://iverb.huckbit.com/api/all')
    return r.json()['data']


def press_or_quit(stdscr):
    """ Waits for key to be pressed. Once it was, checks if it is q """
    ch = stdscr.getkey()

    if ch == 'q':
        exit()


def main(stdscr):
    """ Application entry point """

    curses.curs_set(0)

    curses.start_color()

    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)

    def message(pad, *args):
        """ Decorates global message function to work with scoped stdscr """
        global message
        return message(stdscr, pad, *args)

    pad = curses.newpad(100, 100)

    message(pad, 'Irregular verbs trainer. Press any key to continue. q to quit')

    press_or_quit(stdscr)

    message(pad, 'Fetching data...')

    data = fetch_data()

    message(pad)

    shuffled = list(map(lambda x: (x['infinitive'],
                                   x['past'],
                                   x['pastParticiple']),
                        data))

    shuffle(shuffled)

    for item in shuffled:
        selection = randint(0, 2)

        buffer = ''

        while True:
            test = list(item)
            test[selection] = buffer

            if len(buffer) == 0:
                test[selection] = '?'

            message(pad, ' | '.join(test))

            ch = stdscr.getkey()

            if ch == '\n':
                break

            if ch == 'KEY_BACKSPACE':
                buffer = buffer[:-1]
                continue

            # Should be a character
            if len(ch) != 1:
                continue

            buffer += ch

        correct = False

        # check if multiple variants are correct
        if '/' in item[selection]:
            if buffer in item[selection].split('/'):
                correct = True
        else:
            if buffer == item[selection]:
                correct = True

        status = ('CORRECT', 2) if correct else ('WRONG!', 1)

        output = list(map(lambda x: x + ' ', item))

        output[selection] = (output[selection], 2 if correct else 1)

        message(pad, status, '\n', *output, '\nNext? q to exit')

        press_or_quit(stdscr)


if __name__ == "__main__":
    curses.wrapper(main)
